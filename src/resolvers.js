const { AuthenticationError, UserInputError } = require('apollo-server-express');
const { GraphQLScalarType, Kind } = require('graphql');

module.exports = {
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      console.log(`> parseValue ${value}`);
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value.toUTCString(); // value sent to the client
    },
    parseLiteral(ast) {
      let dateObject;
      if (ast.kind === Kind.INT) {
        dateObject = new Date(Number(ast.value)); // ast value is always in string format
      } else {
        dateObject = new Date(ast.value);
      }
      if (String(dateObject) == 'Invalid Date') {
        throw new UserInputError();
      } else {
        return dateObject;
      }
    },
  }),
  Query: {
    categories: async (_, { filter, orderBy, skip, first }, { dataSources }) => {
      return dataSources.userAPI.findAllCategories({ filter, orderBy, skip, first });
    },
  },
  Mutation: {
    createCategory: async (_, { name }, { dataSources }) => await dataSources.userAPI.createCategory({ name }),
  },
};
