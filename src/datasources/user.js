const { DataSource } = require('apollo-datasource');
const { Op } = require('sequelize');

class UserAPI extends DataSource {
  constructor({ store }) {
    super();
    this.store = store;
  }

  /**
   * This is a function that gets called by ApolloServer when being setup.
   * This function gets called with the datasource config including things
   * like caches and context. We'll assign this.context to the request context
   * here, so we can know about the user making requests
   */
  initialize(config) {
    this.context = config.context;
  }

  async findAllCategories({ filter, orderBy, skip: offset, first: limit }) {
    let order = orderBy == undefined ? orderBy : [orderBy.split('_')];
    let where = {};
    if (filter) {
      if (filter.startCreatedAt) {
        where.createdAt || (where.createdAt = {});
        where.createdAt[Op.gte] = filter.startCreatedAt;
      }
      if (filter.endCreatedAt) {
        where.createdAt || (where.createdAt = {});
        where.createdAt[Op.lt] = filter.endCreatedAt;
      }
      if (filter.name) {
        where.name = { [Op.like]: `%${filter.name}%` };
      }
    }

    return await this.store.Category.findAll({ where, order, offset, limit });
  }

  async createCategory({ name }) {
    return await this.store.Category.create({ name });
  }
}

module.exports = UserAPI;
