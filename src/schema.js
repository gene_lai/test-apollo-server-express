const { gql } = require('apollo-server-express');

module.exports = gql`
  scalar Date

  type Query {
    categories(filter: CategoryFilterInput, orderBy: CategoryOrderByInput, skip: Int, first: Int): [Category]
  }

  type Mutation {
    createCategory(name: String!): Category
  }

  type Category {
    name: String
    createdAt: Date
    updatedAt: Date
  }

  enum CategoryOrderByInput {
    name_ASC
    name_DESC
    createdAt_ASC
    createdAt_DESC
    updatedAt_ASC
    updatedAt_DESC
  }

  input CategoryFilterInput {
    name: String
    startCreatedAt: Date
    endCreatedAt: Date
  }
`;
