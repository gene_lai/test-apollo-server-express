const Sequelize = require('sequelize');

let options = {};
if (process.env.NODE_ENV == 'production') {
  options = { logging: false };
}

const db = new Sequelize('mysql://root:example@localhost:3306/jna@dev', options);

module.exports.createStore = () => {
  const Category = db.define('category', {
    name: {
      type: Sequelize.STRING,
    },
  });

  return { db, Category };
};
