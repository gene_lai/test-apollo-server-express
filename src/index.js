const express = require('express');
const { ApolloServer } = require('apollo-server-express');

const { createStore } = require('./utils');

const UserAPI = require('./datasources/user');

// creates a sequelize connection once. NOT for every request
const store = createStore();

// set up any dataSources our resolvers need
const dataSources = () => ({
  userAPI: new UserAPI({ store }),
});

const server = new ApolloServer({
  typeDefs: require('./schema'),
  resolvers: require('./resolvers'),
  dataSources,
  formatError: (err) => {
    console.log(`formatError:${JSON.stringify(err, null, 2)}`);
    // Don't give the specific errors to the client.
    if (err.message.startsWith('Database Error: ')) {
      return new Error('Internal server error');
    }

    // Otherwise return the original error.  The error can also
    // be manipulated in other ways, so long as it's returned.
    return err;
  },
});

const app = express();
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () =>
  console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath} with environment ${process.env.NODE_ENV}`)
);
