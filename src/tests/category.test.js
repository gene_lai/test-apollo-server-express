const Sequelize = require('sequelize');
const { GraphQLClient } = require('graphql-request');
const API_URL = 'http://localhost:4000/graphql';

const { createStore } = require('../utils');
let { db, Category } = createStore();

beforeAll(async () => {
  await db.sync({ force: true });
});

afterAll(() => {
  db.close();
});

it('createCategory name-1', async () => {
  let result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    mutation {
      createCategory(name: "name-1") {
        name
      }
    }
  `);

  return expect(result).toMatchSnapshot();
});

it('categories', async () => {
  let result = await new GraphQLClient(API_URL).request(/* GraphQL */ `
    query {
      categories(orderBy: createdAt_ASC, first: 3, skip: 1) {
        name
      }
    }
  `);

  return expect(result).toMatchSnapshot();
});
